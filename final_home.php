<?php
		session_start();

?><!DOCTYPE html><html lang="en" class="no-js"><head><meta charset="utf-8"><title>Project</title>
<?php
      if(isset($_SESSION['login']))  {
        echo "<style>.menu-unloged{display:none;}.menu-loged{display:initial;}</style>";
      }
      if(!isset($_SESSION['login']))  {
        echo "<style>.menu-unloged{display:initial;}.menu-loged{display:none;}</style>";
      }
?>
<meta name="description" content=""><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta property="og:image" content="path/to/image.jpg"><link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon"><link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png"><link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png"><link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"><link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css"><link rel="stylesheet" href="css/bootstrap.min.css"><style></style><!-- Load CSS & WebFonts Main Function-->
<link rel="stylesheet" href="css/fonts.min.css"><link rel="stylesheet" href="css/main.min.css"><link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"><script src="js/jquery-3.1.1.min.js"></script></head><body>

<header><div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-7"><a href="final_home.php"><img src="img/logo.png" alt="uknow" class="logo_img"></a></div><div class="col-md-3"><nav>

<ul class="nav_menu menu-unloged"><li><a href="final_home.php">Home</a></li><li><a href="final_login.php">Log In</a><span>/</span><a href="final_registration.php">Registration</a></li></ul>

<ul class="nav_menu menu-loged"><li><a href="final_home.php">Home</a></li><li><a href="php/logout.php">Log Out</a><span>/</span><?php echo $_SESSION['login']; ?></li></ul>

</nav></div></div></div></header>

<main><div class="container"><div class="row"><div class="col-md-3">

<aside class="categories">
<h2>All categories</h2>
<div class="content">
  <div class="categorie"><a href="#">Modern art</a></div>
  <div class="categorie"><a href="#">Simple scetches</a></div>
  <div class="categorie"><a href="#">Pop art</a></div>
  <div class="categorie"><a href="#">Calligraphy</a></div>
  <div class="categorie"><a href="#">Street art</a></div>
  <div class="categorie"><a href="final_categorie_acryl.php">Acryl</a></div>
</div>
</aside>
</div>
<div class="col-md-6"><article><h1>Hot topic this week!</h1><h2>Super Puper Mega POST!</h2><p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, possimus dolores quas aut sequi mollitia, sapiente est non tempora accusantium voluptas laboriosam cum nesciunt accusamus necessitatibus in consequuntur, quasi molestiae.</p><figure><img src="img/forest1.jpg" alt="image" class="topic_img"></figure><p><a href="final_post_super.php">Read more...</a></p></article></div><div class="col-md-3">

<aside class="last_posts"><a href="#"><h2>Last posts</h2></a>
  <div class="content">
    <div class="post">
      <div class="post-heading"><a href="final_post_best.php"><h3>Best post ever!</h3></a></div>
      <div class="post-author">Michael Gukinhauer</div>
      <div class="post-categorie"><a href="final_categorie_acryl.php">Acryl</a></div>
      <p class="post-text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam impedit doloremque molestias, quod maiores, necessitatibus itaque officia et molestiae dolorem repudiandae deleniti inventore aliquam, animi fugiat fuga, ex amet libero?</p>
    </div>

  <div class="post"><div class="post-heading"><a href="final_post_besten.php"><h3>Besten oder nicht!</h3></a></div><div class="post-author">Emma Braun</div><div class="post-categorie"><a href="#">Simple scetches</a></div><p class="post-text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam impedit doloremque molestias, quod maiores, necessitatibus itaque officia et molestiae dolorem repudiandae deleniti inventore aliquam, animi fugiat fuga, ex amet libero?</p></div>

<div class="post"><div class="post-heading"><a href="final_post_how.php"><h3>How is it possible?</h3></a></div><div class="post-author">Demetrius Ari</div><div class="post-categorie"><a href="#">Street art</a></div><p class="post-text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam impedit doloremque molestias, quod maiores, necessitatibus itaque officia et molestiae dolorem repudiandae deleniti inventore aliquam, animi fugiat fuga, ex amet libero?</p></div>
</div>
</aside></div></div></div></main>


<footer><div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-8"><div class="col-md-1"></div><div class="col-md-4"><div class="print_wrapper"><h3>STAY CONNECTED</h3><div class="icons"><a href="https://www.facebook.com/"><i class="fa fa-facebook-official"></i></a><a href="https://www.twitter.com"><i class="fa fa-twitter-square"></i></a><a href="https://www.gmail.com"><i class="fa fa-envelope"></i></a><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></div></div></div><div class="col-md-7"><div class="footer-main"><a href="#">Privacy Policy</a><a href="#">Sitemap</a><p>&copy; Shkarupa Nikita 2016</p></div></div></div><div class="col-md-2"></div></div></div></footer><script src="js/common.js"></script></body></html>
