<?php
    session_start();
?>
<!DOCTYPE html><html lang="en" class="no-js">
<head><meta charset="utf-8"><title>Project</title>
<?php
  if(isset($_SESSION['login_error']))  {
    $GLOBALS['login_error'] = "Login or password is wrong:(";
  }
  else {
   echo "<style>.error{display:none;}</style>";
 }
?>
<meta name="description" content=""><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta property="og:image" content="path/to/image.jpg"><link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon"><link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png"><link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png"><link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"><link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css"><link rel="stylesheet" href="css/bootstrap.min.css"><link rel="stylesheet" href="css/fonts.min.css"><link rel="stylesheet" href="css/main.min.css"><link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"><script src="js/jquery-3.1.1.min.js"></script></head>

<body>

<header><div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-7"><a href="final_home.php"><img src="img/logo.png" alt="uknow" class="logo_img"></a></div><div class="col-md-3"><nav><ul class="nav_menu"><li><a href="final_home.php">Home</a></li><li><a href="final_login.php">Log In</a><span>/</span><a href="final_registration.php">Registration</a></li></ul></nav></div></div></div></header>

<div class="regform">
  <form name="loginForm" action="php/login.php" onsubmit="return LoginFormValidation()" method="post">
    <h1>Enter your login and pass</h1>
    <div class="form_fields">
      <div class="form_field">
        <label for="login"></label>
        <input type="text" name="login" id="login" placeholder="Login" <?php echo @$data['login']; ?> required></div>
        <div class="form_field">
          <label for="password"></label>
          <input type="password" id="password" name="password" placeholder="Password" required></div>
          <div class="form_field">
            <p>
              <a href="#">Forget you password?</a>
            </p>
            <button type="submit" name="submit" class="btn-submit">Log In</button>
          </div>
        </div>
        <?php
          if(isset($GLOBALS['login_error']))  {
            echo '<div class="error"><h2>'.$GLOBALS['login_error'].'</h2></div>';
          };
          unset($_SESSION['login_error']);
        ?>
      </form>
    </div>

<footer><div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-8"><div class="col-md-1"></div><div class="col-md-4"><div class="print_wrapper"><h3>STAY CONNECTED</h3><div class="icons"><a href="https://www.facebook.com/"><i class="fa fa-facebook-official"></i></a><a href="https://www.twitter.com"><i class="fa fa-twitter-square"></i></a><a href="https://www.gmail.com"><i class="fa fa-envelope"></i></a><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></div></div></div><div class="col-md-7"><div class="footer-main"><a href="#">Privacy Policy</a><a href="#">Sitemap</a><p>&copy; Shkarupa Nikita 2016</p></div></div></div><div class="col-md-2"></div></div></div></footer><script src="js/common.js"></script
body></html>
