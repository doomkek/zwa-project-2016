<?php
  session_start();

  checkUser();

  function authorized() {
    $_SESSION['login'] = $_POST['login'];
  }

  function checkUser() {
    if(isset($_POST['login'])){
      $_POST['login'] = htmlspecialchars($_POST['login']);
    };
    $login = $_POST['login'];
    if(isset($_POST['password'])){
      $_POST['password'] = htmlspecialchars($_POST['password']);
    };
    $pass = $_POST['password'];
    $names=file('../data/users.txt');
    foreach ($names as $name) {
      $test = explode(":", $name);
      $hash = trim($test[1]);
      if (trim($test[0]) == trim($login) and password_verify($pass, $hash)) {
        authorized();
        $GLOBALS['error'] = 0;
        header("Location: ../final_home.php");
        break;
      }
    }
    if (!isset($GLOBALS['error'])) {
      $_SESSION['login_error'] = "Login or password is wrong:(";
      header("Location: ../final_login.php");
      }
    }
?>
