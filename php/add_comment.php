<?php
  session_start();

  $login = $_SESSION['login'];
  if(isset($_POST['comment_text'])){
    $_POST['comment_text'] = htmlspecialchars($_POST['comment_text']);    
  }
  $comment = $_POST['comment_text'];
  $comment = str_replace("\n", " ", $comment);
  $comment = str_replace("\r", "", $comment);
  $text = "$login~$comment\r\n";
  $file = fopen('../data/comments_best.txt', "a");
  fputs($file, $text);
  fclose($file);
  header("Location: {$_SERVER['HTTP_REFERER']}");
?>
