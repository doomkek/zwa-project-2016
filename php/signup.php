<?php
  session_start();

  $GLOBALS['error'] = 0;

  checkUser();

  function authorized() {
    $_SESSION['login'] = $_POST['login'];
  }

  function addUser() {
    if(isset($_POST['login'])){
      $_POST['login'] = htmlspecialchars($_POST['login']);
    };
    $login = $_POST['login'];
    if(isset($_POST['password'])){
      $_POST['password'] = htmlspecialchars($_POST['password']);
    };
    $pass = $_POST['password'];
    $pass = password_hash($pass, PASSWORD_DEFAULT);
    $file = fopen('../data/users.txt', "a");
    $text="$login:$pass\r\n";
    fputs($file, $text);
    fclose($file);
  }

  function checkUser() {
    if(isset($_POST['login'])){
      $_POST['login'] = htmlspecialchars($_POST['login']);
    };
    $login = $_POST['login'];
    if(isset($_POST['password'])){
      $_POST['password'] = htmlspecialchars($_POST['password']);
    };
    $pass = $_POST['password'];
    $names=file('../data/users.txt');
    foreach ($names as $name)
      {
        $test = explode(":", $name);
        if ($test[0] == $login) {
          $GLOBALS['error'] = 1;
          $_SESSION['signup_error'] = "Login or password is wrong:(";
          header("Location: ../final_registration.php");
          break;
        }
      }
    if ($GLOBALS['error'] == 0) {
          addUser();
          authorized();
          header("Location: ../final_home.php");
        }
      }
?>
