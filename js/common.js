//  Log in form appearance
function PopUpShow(){
  $("#popup1").show();
}
function PopUpHide(){
  $("#popup1").hide();
}

//  Form validation
// Log in
function LoginFormValidation(){
  var a = document.forms["loginForm"]["email"].value;
  if (a == "") {
    alert("Try again, email is missing:(");
    return false;
  }
  var b = document.forms["loginForm"]["pass"].value;
  if (b == "") {
    alert("Try again, password is missing:(");
    return false;
  }
}
// Registration
function RegFormValidation(){
  var a = document.forms["regForm"]["login"].value;
  if (a == "") {
    alert("Try again, login is missing:(");
    return false;
  }
  var b = document.forms["regForm"]["email"].value;
  if (b == "") {
    alert("Try again, email is missing:(");
    return false;
  }
  var c = document.forms["regForm"]["password"].value;
  if (c == "") {
    alert("Try again, password is missing:(");
    return false;
  }
}

// Pagination
// $( document ).ready(function() {
//     document.getElementById("6").style.display = "none";
//     document.getElementById("7").style.display = "none";
//     document.getElementById("8").style.display = "none";
//     document.getElementById("9").style.display = "none";
//     document.getElementById("10").style.display = "none";
// });
// $('#page_1').click(function() {
//     document.getElementById("1").style.display = "initial";
//     document.getElementById("2").style.display = "initial";
//     document.getElementById("3").style.display = "initial";
//     document.getElementById("4").style.display = "initial";
//     document.getElementById("5").style.display = "initial";
//     document.getElementById("6").style.display = "none";
//     document.getElementById("7").style.display = "none";
//     document.getElementById("8").style.display = "none";
//     document.getElementById("9").style.display = "none";
//     document.getElementById("10").style.display = "none";
// });
// $('#page_2').click(function() {
//     document.getElementById("1").style.display = "none";
//     document.getElementById("2").style.display = "none";
//     document.getElementById("3").style.display = "none";
//     document.getElementById("4").style.display = "none";
//     document.getElementById("5").style.display = "none";
//     document.getElementById("6").style.display = "initial";
//     document.getElementById("7").style.display = "initial";
//     document.getElementById("8").style.display = "initial";
//     document.getElementById("9").style.display = "initial";
//     document.getElementById("10").style.display = "initial";
// });

$(document).ready(function(){
  var div = document.getElementById("counter");
  var max = div.textContent + 1;
  var i;
  for (i = 6; i < max; i++) {
    $("#" + i).hide();
  };
  $('.page-link').click(function() {
    var page = $(this).attr('id');
    page = parseInt(page.substring(5), 10);
    var offset = (page - 1)*5;
    var a;
    for (a = 1; a < max; a++) {
      $("#" + a).hide();
    };
    for (a = offset + 1; a < offset + 6; a++) {
      $("#" + a).show();
    };
  });
});
