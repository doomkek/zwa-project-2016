<?php
    session_start();
?>
	<!DOCTYPE html>
	<html lang="en" class="no-js">

	<head>
		<meta charset="utf-8">
		<title>Project</title>
		<?php
  if(isset($_SESSION['signup_error']))  {
    $GLOBALS['signup_error'] = "This login is already exist:(";
  }
  else {
   echo "<style>.error{display:none;}</style>";
 }
?>
			<meta name="description" content="">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<meta property="og:image" content="path/to/image.jpg">
			<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
			<link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
			<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
			<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
			<link rel="stylesheet" href="css/bootstrap.min.css">
			<style></style>
			<!-- Load CSS & WebFonts Main Function-->
			<link rel="stylesheet" href="css/fonts.min.css">
			<link rel="stylesheet" href="css/main.min.css">
			<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
			<script src="js/jquery-3.1.1.min.js"></script>
	</head>

	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-7">
						<a href="final_home.php"><img src="img/logo.png" alt="uknow" class="logo_img"></a>
					</div>
					<div class="col-md-3">
						<nav>
							<ul class="nav_menu">
								<li><a href="final_home.php">Home</a></li>
								<li><a href="final_login.php">Log In</a><span>/</span><a href="final_registration.php">Registration</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<main>
			<div class="container">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="regform">
							<h1 class="heading">Join our forum today.</h1>

							<form name="regForm" action="php/signup.php" method="post" onsubmit="return RegFormValidation()">
								<div class="form_fields">
									<div class="form_field"><label for="user_name"></label><input type="text" name="login" id="user_name" placeholder="Username (min 6 signs)" pattern="[a-zA-Z0-9]{6,20}" <?php echo @$data[ 'login']; ?> required></div>
									<div class="form_field"><label for="pass"></label><input type="password" id="pass" name="password" placeholder="Password (min 6 signs)" pattern="(.{6,})" required></div>
									<div class="form_field">
										<div class="checkbox"><input id="check" type="checkbox" value="None" required><label for="check"></label>
											<p>I agree with forum<a href="#"> rules.</a></p><button type="submit" name="submit" class="btn-submit">End registration</button></div>
									</div>
									<?php
if (!isset($GLOBALS['signup_error'])) {
  $GLOBALS['signup_error'] = "";
}else {
  echo '<div class="error"><h2>'.$GLOBALS['signup_error'].'</h2></div>';
  unset($_SESSION['signup_error']);
};
?>
								</div>
							</form>


						</div>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</main>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<div class="print_wrapper">
								<h3>STAY CONNECTED</h3>
								<div class="icons"><a href="https://www.facebook.com/"><i class="fa fa-facebook-official"></i></a><a href="https://www.twitter.com"><i class="fa fa-twitter-square"></i></a><a href="https://www.gmail.com"><i class="fa fa-envelope"></i></a><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="footer-main"><a href="#">Privacy Policy</a><a href="#">Sitemap</a>
								<p>&copy; Shkarupa Nikita 2016</p>
							</div>
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</footer>
		<script src="js/common.js"></script>
	</body>

	</html>
