<?php
 session_start();
?><!DOCTYPE html><html lang="en" class="no-js"><head><meta charset="utf-8"><title>Project</title>
<?php
      if(isset($_SESSION['login']))  {
        echo "<style>.menu-unloged{display:none;}.menu-loged{display:initial;}</style>";
      }
      if(!isset($_SESSION['login']))  {
        echo "<style>.menu-unloged{display:initial;}.menu-loged{display:none;}</style>";
      }
      if(!isset($_SESSION['login']))  {
        echo "<style>.leave_a_comment{display:none;}#delete{display:none;}</style>";
      }
?>
<style>.display-none{display: none;}</style>
<meta name="description" content=""><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta property="og:image" content="path/to/image.jpg"><link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon"><link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png"><link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png"><link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"><link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css"><link rel="stylesheet" href="css/bootstrap.min.css"><style></style><link rel="stylesheet" href="css/fonts.min.css"><link rel="stylesheet" href="css/main.min.css"><link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"><script src="js/jquery-3.1.1.min.js"></script></head>

<body><header><div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-7"><a href="final_home.php"><img src="img/logo.png" alt="uknow" class="logo_img"></a></div><div class="col-md-3">

<nav>

<ul class="nav_menu menu-unloged"><li><a href="final_home.php">Home</a></li><li><a href="final_login.php">Log In</a><span>/</span><a href="final_registration.php">Registration</a></li></ul>

<ul class="nav_menu menu-loged"><li><a href="final_home.php">Home</a></li><li><a href="php/logout.php">Log Out</a><span>/</span><?php echo $_SESSION['login']; ?></li></ul>

</nav>

</div></div></div></header><main><div class="container"><div class="row"><div class="col-md-3"><aside class="categories"><h2>All categories</h2>

<div class="content"><div class="categorie"><a href="#">Modern art</a></div><div class="categorie"><a href="#">Simple scetches</a></div><div class="categorie"><a href="#">Pop art</a></div><div class="categorie"><a href="#">Calligraphy</a></div><div class="categorie"><a href="#">Street art</a></div><div class="categorie"><a href="final_categorie_acryl.php">Acryl</a></div></div>

</aside></div><div class="col-md-9"><div class="post_wrapper"><h1 class="post_heading">Best post ever!</h1><h2 class="post_author">Michael Gukinhauer</h2><p class="post_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur quis neque cupiditate eaque provident ullam temporibus repellendus voluptates tempore delectus minima, ex sequi dolore cum esse odio, reiciendis sapiente. Commodi!</p><figure><img src="img/Acryl-blue.jpg" alt="image" class="topic_img"></figure></div>

<div class="comments_wrapper" id="comments">

<h2>Comments</h2>

<div class="comments-pagination">
<?php
  $file = file('data/comments_best.txt');
  if (!isset($GLOBALS['page'])) {
    $GLOBALS['page'] = 1;
  };
  if (isset($_POST['value'])) {
    $GLOBALS['page'] = $_POST['value'];
  };
  $page = $GLOBALS['page'];
  $b = (($page-1)*5) + 5;
  if ($b > count($file)) {
    $b = count($file);
  };
  for ($i=(($page-1)*5); $i<$b; $i++) {
    $data = explode("~", $file[$i]);
    $login = $data[0];
    $GLOBALS['login'] = $login;
    $comment = $data[1];
    ?>
      <div class="comment row" id="<?php echo  $i; ?>"><div class="col-md-3 title"><p class="comment_author">
      <?php
        echo $login;
      ?>
      </p>
      <?php
        if (isset($_SESSION['login']) and $_SESSION['login'] == $GLOBALS['login']) {
        ?>
      <input id ="delete" type="submit" name="delete_comment" value="Delete">
      <?php
        };
      ?>
      </div><div class="col-md-9 comment_content"><p>
      <?php
        echo $comment;
      ?>
      </p></div></div>
      <?php
      };
?>
</div>

<nav>
  <ul class="pagination">
    <?php
      $file = file('data/comments_best.txt');
      $a = count($file);
      if ($a % 5 == 0){
        $a = $a / 5;
      } else {
        $a = $a / 5 + 1;
      }
      for ($i = 1; $i <= $a; $i++) {
    ?>
      <form name="pagination" method="post">
        <input type="text" class="display-none" name="value" value="<?php echo $i;?>">
        <button class="page-item, page-link" id="<?php echo $i;?>">
          <?php echo $i;?>
        </button>
      </form>
    <?php
      };
    ?>
  </ul>
</nav>

<div class="leave_a_comment"><h3>Leave your comment:</h3><form id="comment" method="post" action="php/add_comment.php"><label for="comment_input"></label><textarea id="comment_input" name="comment_text" cols="50" rows="6" maxlength="800" required></textarea><p></p><input type="submit" name="save" value="Post"></form></div></div>


</div></div></div></main><footer><div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-8"><div class="col-md-1"></div><div class="col-md-4"><div class="print_wrapper"><h3>STAY CONNECTED</h3><div class="icons"><a href="https://www.facebook.com/"><i class="fa fa-facebook-official"></i></a><a href="https://www.twitter.com"><i class="fa fa-twitter-square"></i></a><a href="https://www.gmail.com"><i class="fa fa-envelope"></i></a><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></div></div></div><div class="col-md-7"><div class="footer-main"><a href="#">Privacy Policy</a><a href="#">Sitemap</a><p>&copy; Shkarupa Nikita 2016</p></div></div></div><div class="col-md-2"></div></div></div></footer><script src="js/common.js"></script></body></html>
