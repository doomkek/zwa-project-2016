<?php
    session_start();

?><!DOCTYPE html><html lang="en" class="no-js"><head><meta charset="utf-8"><title>Project</title>
<?php
      if(isset($_SESSION['login']))  {
        echo "<style>.menu-unloged{display:none;}.menu-loged{display:initial;}</style>";
      }
      if(!isset($_SESSION['login']))  {
        echo "<style>.menu-unloged{display:initial;}.menu-loged{display:none;}</style>";
      }
?>
<meta name="description" content="Project ZWA, uknow forum"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta property="og:image" content="path/to/image.jpg">
<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/fonts.min.css">
<link rel="stylesheet" href="css/main.min.css">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<script src="js/jquery-3.1.1.min.js"></script></head>

<body>
<header>
<div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-7"><a href="final_home.php"><img src="img/logo.png" alt="uknow" class="logo_img"></a></div><div class="col-md-3"><nav>

<ul class="nav_menu menu-unloged"><li><a href="final_home.php">Home</a></li><li><a href="final_login.php">Log In</a><span>/</span><a href="final_registration.php">Registration</a></li></ul>

<ul class="nav_menu menu-loged"><li><a href="final_home.php">Home</a></li><li><a href="php/logout.php">Log Out</a><span>/</span><?php echo $_SESSION['login']; ?></li></ul>

</nav></div></div></div></header>

<main>
<div class="container categorie_wrapper"><div class="row"><div class="col-md-3"><aside class="categories"><h2>All categories</h2>
<div class="content"><div class="categorie"><a href="#">Modern art</a></div><div class="categorie"><a href="#">Simple scetches</a></div><div class="categorie"><a href="#">Pop art</a></div><div class="categorie"><a href="#">Calligraphy</a></div><div class="categorie"><a href="#">Street art</a></div><div class="categorie"><a href="final_categorie_acryl.php">Acryl</a></div></div>

</aside></div><div class="col-md-9"><h1>Acryl</h1><div class="post_wrapper"><h2 class="post_heading"><a href="final_post_best.php">Best post ever!</a></h2><h3 class="post_author">Michael Gukinhauer</h3><p class="post_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur quis neque cupiditate eaque provident ullam temporibus repellendus voluptates tempore delectus minima, ex sequi dolore cum esse odio, reiciendis sapiente. Commodi!</p></div><div class="post_wrapper"><h2 class="post_heading"><a href="final_post_super.php">Super Puper Mega POST!</a></h2><h3 class="post_author">Mnason Sandip</h3><p class="post_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur quis neque cupiditate eaque provident ullam temporibus repellendus voluptates tempore delectus minima, ex sequi dolore cum esse odio, reiciendis sapiente. Commodi!</p></div><div class="post_wrapper"><h3 class="post_author">Usefull articles :</h3><a href="https://en.wikipedia.org/wiki/Acrylic_paint">Acrylic painting (wikipedia)</a><p class="post_content">Acrylic paint is a fast-drying paint made of pigment suspended in acrylic polymer emulsion. Acrylic paints are water-soluble, but become water-resistant when dry. Depending on how much the paint is diluted with water, or modified with acrylic gels, media, or pastes, the finished acrylic painting can resemble a watercolor or an oil painting, or have its own unique characteristics not attainable with other media.</p><a href="https://ru.pinterest.com/explore/%D0%B0%D0%BA%D1%80%D0%B8%D0%BB%D0%BE%D0%B2%D0%B0%D1%8F-%D0%B6%D0%B8%D0%B2%D0%BE%D0%BF%D0%B8%D1%81%D1%8C-929280698285/">Just cool arts (pinterest)</a></div></div></div></div></main>

<footer><div class="container"><div class="row"><div class="col-md-2"></div><div class="col-md-8"><div class="col-md-1"></div><div class="col-md-4"><div class="print_wrapper"><h3>STAY CONNECTED</h3><div class="icons"><a href="https://www.facebook.com/"><i class="fa fa-facebook-official"></i></a><a href="https://www.twitter.com"><i class="fa fa-twitter-square"></i></a><a href="https://www.gmail.com"><i class="fa fa-envelope"></i></a><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></div></div></div><div class="col-md-7"><div class="footer-main"><a href="#">Privacy Policy</a><a href="#">Sitemap</a><p>&copy; Shkarupa Nikita 2016</p></div></div></div><div class="col-md-2"></div></div></div></footer><script src="js/common.js"></script></body></html>
